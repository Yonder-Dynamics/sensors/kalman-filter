import re
import math
import numpy as np

text = open("calib.txt", 'r')

# 698 is how many times accel appears

pos = []
vel = []
accel = []
rot = []
gyro = []

for line in text.readlines():
    #last 3 (x'', y'', z'')
    float_regex = r'([+-]?[0-9]*[.][0-9]+)'
    csv_regex = " *" +float_regex+', *'+float_regex+', *'+float_regex
    match = re.match(r'.*Accel:'+csv_regex, line)
    if match is not None:
        pos.append([0,0,0])
        vel.append([0,0,0])
        accel.append([float(match.group(i+1)) for i in range(3)])
        continue
    match = re.search(r'.*Gyro:'+csv_regex, line)
    if match is not None:
        gyro.append([float(match.group(i+1))*2*math.pi/360 for i in range(3)])
        continue
    match = re.search(r'.*Software yaw, pitch, roll:'+csv_regex, line)
    if match is not None:
        rot.append([float(match.group(i+1))*2*math.pi/360 for i in range(3)])
        continue

accel = np.array(accel)
amax = np.max(accel, axis=0)
amin = np.min(accel, axis=0)
print(np.mean(accel, axis=0))
print(amax, amin)
with open("accel_calib.npz", "wb+") as f:
    np.savez(f, amax=amax, amin=amin, mean=np.mean(accel, axis=0))
