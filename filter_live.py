import os
import re
import math
import numpy as np

from filterpy.kalman import KalmanFilter
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from matplotlib import style
import pyquaternion
import pyzed.sl as sl

# TODO: Remove gravity

def parse(text):
    pos = []
    vel = []
    accel = []
    rot = []
    gyro = []
    quat = []
    float_regex = r'([+-]?[0-9]*[.][0-9]+)'
    csv_regex = " *"+float_regex+', *'+float_regex+', *'+float_regex
    #  quat_regex = r'q0 = {} qx = {} qy = {} qz = {}'.format(*([float_regex]*4))
    quat_regex = r'Q0 = {} Qx = {} Qy = {} Qz = {}'.format(*([float_regex]*4))
    for line in text.readlines():
        #last 3 (x'', y'', z'')
        match = re.match(r'.*Accel:'+csv_regex, line)
        if match is not None:
            pos.append([0,0,0])
            vel.append([0,0,0])
            accel.append([float(match.group(i+1)) for i in range(3)])
            continue
        match = re.match(quat_regex, line)
        if match is not None:
            quat.append(pyquaternion.Quaternion(x=match.group(1), y=match.group(2), z=match.group(3), w=match.group(4)))
            continue
        match = re.search(r'.*Gyro:'+csv_regex, line)
        if match is not None:
            gyro.append([float(match.group(i+1))*2*math.pi/360 for i in range(3)])
            continue
        match = re.search(r'.*Software yaw, pitch, roll:'+csv_regex, line)
        if match is not None:
            rot.append([float(match.group(i+1))*2*math.pi/360 for i in range(3)])
            continue
    return pos, vel, accel, rot, gyro, quat

def zip_measures(accel, rot, gyro):
    # [*sum(zip(l2,l1, l3),())] interleaves 3 sets
    return [list(a)+[*sum(zip(r,g),())] for (a, r, g) in zip(accel, rot, gyro)]

#  def zip_measures(pos, vel, accel, rot, gyro):
#      # [*sum(zip(l2,l1, l3),())] interleaves 3 sets
#      return [[*sum(zip(p,v,a),())]+[*sum(zip(r,g),())] for (p, v, a, r, g) in zip(pos, vel, accel, rot, gyro)]

def load_measurements(text):
    pos, vel, accel, rot, gyro, quat = parse(text)
    with open("accel_calib.npz", "rb") as f:
        calib = np.load(f)
        amin = calib["amin"]
        amax = calib["amax"]
        amean = calib["mean"]

    accel = np.array(accel)
    # Remove gravity
    gravity = np.array([0, 0, 1000])
    sub = np.array([q.rotate(gravity) for q in quat])
    accel -= sub

    # Apply calib and fix units
    offset = (amax + amin) / 2
    print(np.mean(accel, axis=0))
    offset = np.mean(accel, axis=0)
    accel = (accel-offset) * 9.81 / 1000
    print(np.mean(accel, axis=0))

    return np.array(zip_measures(accel, rot, gyro))

with open("very_still.txt", 'r') as text:
    still_run = load_measurements(text)
R = np.cov(still_run.T)
print(R.shape)


with open("imu_test_circle_4.txt", 'r') as text:
#  with open("very_still.txt", 'r') as text:
    measurements = load_measurements(text)

#  print(np.cov(measurements.T).shape)
#  print(measurements[:,2])
#  print(np.std(measurements[:,2]))
#  print(np.std(measurements[:,5]))
#  print(np.std(measurements[:,8]))

#def tracker1():
tracker = KalmanFilter(dim_x=15, dim_z=9)
#dim of z = 9 as we're only getting 9 values as an input from the IMU
dt = 0.05 #time step

#IMU only reads R P Y xAcc yAcc zAcc R' P' Y':
#H needs to be 9x15, where each row is the measurement of R/P/Y/xAcc...,
#                   and each col corresponds to all the 15 variables
# x, x', x'', y, y', y'', z, z', z'', R, R', P, P', Y, Y'
tracker.H = np.array([[0,0,1,  0,0,0,  0,0,0,  0,0, 0,0, 0,0],
                      [0,0,0,  0,0,1,  0,0,0,  0,0, 0,0, 0,0],
                      [0,0,0,  0,0,0,  0,0,1,  0,0, 0,0, 0,0],
                      [0,0,0,  0,0,0,  0,0,0,  1,0, 0,0, 0,0],
                      [0,0,0,  0,0,0,  0,0,0,  0,1, 0,0, 0,0],
                      [0,0,0,  0,0,0,  0,0,0,  0,0, 1,0, 0,0],
                      [0,0,0,  0,0,0,  0,0,0,  0,0, 0,1, 0,0],
                      [0,0,0,  0,0,0,  0,0,0,  0,0, 0,0, 1,0],
                      [0,0,0,  0,0,0,  0,0,0,  0,0, 0,0, 0,1]])

#need to learn relationship of roll pitch yaw with their derivatives
#                      x    x'    x''      y    y'      y''    z     z'       z''   R  R' P  P' Y  Y'
tracker.F = np.array([[1,   dt, .5*dt*dt,  0,   0,      0,     0,    0,       0,    0, 0, 0, 0, 0, 0], #x
                      [0,   1,     dt,     0,   0,      0,     0,    0,       0,    0, 0, 0, 0, 0, 0], #x'
                      [0,   0,     1,      0,   0,      0,     0,    0,       0,    0, 0, 0, 0, 0, 0], #x''
                      [0,   0,     0,      1,   dt, .5*dt*dt,  0,    0,       0,    0, 0, 0, 0, 0, 0], #y
                      [0,   0,     0,      0,   1,      dt,    0,    0,       0,    0, 0, 0, 0, 0, 0], #y'
                      [0,   0,     0,      0,   0,      1,     0,    0,       0,    0, 0, 0, 0, 0, 0], #y''
                      [0,   0,     0,      0,   0,      0,     1,    dt, .5*dt*dt,  0, 0, 0, 0, 0, 0], #z
                      [0,   0,     0,      0,   0,      0,     0,    1,       dt,   0, 0, 0, 0, 0, 0], #z'
                      [0,   0,     0,      0,   0,      0,     0,    0,       1,    0, 0, 0, 0, 0, 0], #z''
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    1, dt, 0, 0, 0, 0], #R  FIND
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    0, 1, 0, 0, 0, 0], #R'
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    0, 0, 1, dt, 0, 0], #P  FIND
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    0, 0, 0, 1, 0, 0], #P'
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    0, 0, 0, 0, 1, dt], #Y   FIND
                      [0,   0,     0,      0,   0,      0,     0,    0,       0,    0, 0, 0, 0, 0, 1]]) #Y''

def disp_raw():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = np.zeros((15,1))
    x[[2, 5, 8, 9, 10, 11, 12, 13, 14]] = measurements[0].reshape(-1, 1)
    xs = [x]
    for row in measurements:
        x = tracker.F.dot(x)
        row = row.reshape(-1, 1)
        x[[2, 5, 8, 9, 10, 11, 12, 13, 14]] = row
        xs.append(x)

    xs = np.array(xs).squeeze()
    ax.plot(xs[:, 0], xs[:, 3], xs[:, 6], c='r')
    ax.plot(xs[:, 1], xs[:, 4], xs[:, 7], c='g')
    #  ax.plot(xs[:, 2], xs[:, 5], xs[:, 8], c='b')
    plt.show()

#x = 15x1; F = 15x5
#--> xline (state) = Fx = 15x1

# process noise matrix
# Cheating. This should be captured at some point by moving the IMU in very precise ways
# Also this should be passed through the state transition matrix
tracker.Q = np.ones((15,15))*1e-5*tracker.F

#R = measurement noise Matrix (covariance of inputs)
tracker.R = R

#iniital conditions covariance

#initial position/input at (0,0,...)
tracker.x = np.zeros(15)

# wtf
tracker.p = np.eye(15) * 1

xms, xPPms = [], []
xs, xPPs, xPs = [], [], []
yPPs, zPPs = [], []
ys = []
zs = []
msX = []
msY = []
msZ = []

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.interactive(False)
print("Running filter")

def init_zed():
    # Init ZED Camera
    zed = sl.Camera()
    init_params = sl.InitParameters()
    # Use HD720 video mode (default fps: 60)
    init_params.camera_resolution = sl.RESOLUTION.RESOLUTION_HD720
    # Use a right-handed Y-up coordinate system
    init_params.coordinate_system = sl.COORDINATE_SYSTEM.COORDINATE_SYSTEM_RIGHT_HANDED_Y_UP
    init_params.coordinate_units = sl.UNIT.UNIT_METER  # Set units in meters

    err = zed.open(init_params)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Enable positional tracking with default parameters
    py_transform = sl.Transform()  # First create a Transform object for TrackingParameters object
    tracking_parameters = sl.TrackingParameters(init_pos=py_transform)
    err = zed.enable_tracking(tracking_parameters)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Track the camera position during 1000 frames
    i = 0
    zed_pose = sl.Pose()
    zed_imu = sl.IMUData()
    runtime_parameters = sl.RuntimeParameters()
    return zed_pose, zed_imu

zed_pose, zed_imu = init_zed()

while True:
    if zed.grab(runtime_parameters) != sl.ERROR_CODE.SUCCESS:
        continue
    # Get the pose of the left eye of the camera with reference to the world frame
    zed.get_position(zed_pose, sl.REFERENCE_FRAME.REFERENCE_FRAME_WORLD)
    zed.get_imu_data(zed_imu, sl.TIME_REFERENCE.TIME_REFERENCE_IMAGE)

    # Display the translation and timestamp
    py_translation = sl.Translation()
    trans = zed_pose.get_translation(py_translation).get()
    time = zed_pose.timestamp

    py_orientation = sl.Orientation()
    zed_ori = zed_pose.get_orientation(py_orientation).get()

    #Display the IMU acceleratoin
    acceleration = [0,0,0]
    zed_imu.get_linear_acceleration(acceleration)

    #Display the IMU angular velocity
    a_velocity = [0,0,0]
    zed_imu.get_angular_velocity(a_velocity)

    # Display the IMU orientation quaternion
    imu_orientation = sl.Orientation()
    imu_ori = zed_imu.get_orientation(imu_orientation).get()
    print(zed_ori, imu_ori)

    # Vim 
    tracker.predict()
    tracker.update(z)
    xPPs.append(tracker.x[0]) #accel x
    yPPs.append(tracker.x[2])
    zPPs.append(tracker.x[5])
    xs.append(tracker.x[0]) #regular x
    ys.append(tracker.x[3])
    zs.append(tracker.x[6]) #the z values (x, y, z) -- not measurements 
ax.plot(zs, xs, ys)
    #  ax.plot(xPPs, yPPs, zPPs)
    #  msX.append(z[2]) #accel, not regular x
    #  msY.append(z[5])
    #  msZ.append(z[8])
    #  ax.plot(msX, msY, msZ)
    #  plt.draw()
    #  plt.pause(0.0001)
    #  ax.cla()
plt.show()


#xs, cov = np.array(xs), np.array(cov)
#plot_measurements(mu[:, 2], zs[:, 2])
#print(xs[0])


y = np.abs(z - np.dot(tracker.H, tracker.x))
dist = np.linalg.norm(y)

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.plot(xs, ys, Zs)

#plt.plot(xs) #blue
#plt.plot(xPPs) #orange
#plt.plot(xPs) #green
#plt.plot(mu[0])
#plt.show()

#print(zs[0])
#print(xs[0])

'''
# plot results
plot_filter(mu[:, 0], mu[:, 2])
plot_measurements(zs[:, 0], zs[:, 1])
plt.legend(loc=2)
plt.xlim(0, 20)
plt.show()
'''
