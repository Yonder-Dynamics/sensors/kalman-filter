import argparse
import redis
import yaml
import serial
import time
import os
import calibrate
import parse
import pyzed.sl as sl
import numpy as np
from pyquaternion import Quaternion
from kalman_filter import sigma_points, UKF

ns = 1e9
ms = 1e6
separator = b'0====D~'

parser = argparse.ArgumentParser(description="IMU driver")
parser.add_argument('--port', help='Serial port for IMU')
parser.add_argument('--baudrate', help='Serial baudrate')
parser.add_argument('--calibration', help='Load calibration file', default=None)
parser.add_argument('--redis_host', help='Redis host server ip')
parser.add_argument('--redis_port', help='Redis server port', default=6379)
parser.add_argument('--config', help='Config for all args')
parser.add_argument('--imu_topic', help='Redis topic to publish data on', default='/imu')
#parser.add_argument('--calibrate', help='Run calibration', default=None)

#         0 1  2    3 4  5    6 7  8      9  10     11    12    13   14
# State: [x x' x'', y y' y'', z z' z'', yaw yaw' pitch pitch' roll roll']
# State transition matrix

def yaw_pitch_roll_to_quat(yaw, pitch, roll):
    # y up, z front, x right
    return Quaternion(axis=np.array([1, 0, 0]), radians=pitch) * \
           Quaternion(axis=np.array([0, 1, 0]), radians=yaw) * \
           Quaternion(axis=np.array([0, 0, 1]), radians=roll)

def state_transition(x, dt):
    accel_int = np.array([[1, dt, dt**2/2],
                          [0, 1, dt],
                          [0, 0, 1]])
    vel_int = np.array([[1, dt],
                        [0, 1]])
    st = np.block([accel_int]*3 + [vel_int]*3)
    return st.dot(x)

# The sensor translational data is within the local frame while we want to track the global frame
# Therefore we have to rotate the translation from the local to global frame by applying the inverse of the orientation
# IMU Output: [x'' y'' z'' yaw pitch roll]
def state_to_imu(x):
    # add gravity
    # translate the acceleration to local frame of reference based on the orientation
    rot = yaw_pitch_roll_to_quat(x[9], x[11], x[13]).inverse()
    accel = rot.rotate(np.array([x[2], x[5], x[8]]))
    return np.array([accel[0], accel[1], accel[2], x[9], x[11], x[13]])

# VO Output: [x' y' z' yaw' pitch' roll']
def state_to_vo(x):
    # translate the acceleration to local frame of reference based on the orientation
    rot = yaw_pitch_roll_to_quat(x[9], x[11], x[13]).inverse()
    vel = rot.rotate(np.array([x[1], x[4], x[7]]))
    return np.array([vel[0], vel[1], vel[2], x[10], x[12], x[14]])

def init_UKF():
    points = sigma_points.MerweScaledSigmaPoints(n=2, alpha=.3, beta=2., kappa=.1)
    # we need to keep track of R
    ukf = UKF(18-3, 6, dt=None, hx=state_to_imu, fx=state_transition, points=points)
    return ukf

def init_zed():
    # Init ZED Camera
    zed = sl.Camera()
    init_params = sl.InitParameters()
    # Use HD720 video mode (default fps: 60)
    init_params.camera_resolution = sl.RESOLUTION.RESOLUTION_HD720
    # Use a right-handed Y-up coordinate system
    init_params.coordinate_system = sl.COORDINATE_SYSTEM.COORDINATE_SYSTEM_RIGHT_HANDED_Y_UP
    init_params.coordinate_units = sl.UNIT.UNIT_METER  # Set units in meters

    err = zed.open(init_params)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Enable positional tracking with default parameters
    py_transform = sl.Transform()  # First create a Transform object for TrackingParameters object
    tracking_parameters = sl.TrackingParameters(init_pos=py_transform)
    tracking_parameters.enable_spatial_memory = False
    tracking_parameters.enable_imu_fusion = False
    err = zed.enable_tracking(tracking_parameters)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Track the camera position during 1000 frames
    i = 0
    zed_pose = sl.Pose()
    zed_imu = sl.IMUData()
    runtime_parameters = sl.RuntimeParameters()
    return zed, zed_pose, zed_imu, runtime_parameters

def parse_args(parser):
    args = parser.parse_args()
    if args.config:
        with open(args.config, "r") as f:
            data = yaml.load(f)
            print(data)
            delattr(args, 'config')
            arg_dict = args.__dict__
            for key, value in data.items():
                if isinstance(value, list):
                    for v in value:
                        arg_dict[key].append(v)
                else:
                    arg_dict[key] = value
    return args

def read_imu():
    byte_dat = ser.read_until(separator)[:-len(separator)]
    dat = parse.parse_raw(byte_dat)
    trans = dat[:3]
    gyro = dat[3:6]
    ori_q = Quaternion(w=dat[12], x=dat[9], y=dat[10], z=dat[11])
    time = dat[13]
    #print(quat)
    ori = np.array(quat.yaw_pitch_roll)
    return trans, gyro, ori, time

args = parse_args(parser)
ser = serial.Serial(args.port, args.baudrate)
time.sleep(0.1) # BUGS BEGONE. Do not remove
#  r = redis.Redis(host=args.redis_host, port=args.redis_port, db=0)
calibrator = calibrate.load(args.calibration) # currently identity
zed, zed_pose, zed_imu, runtime_parameters = init_zed()

py_translation = sl.Translation()
p_time_zed = zed_pose.timestamp
_, _, _, p_time_imu = read_imu()
ukf = init_UKF()

# TODO Add real covariances
# To do this properly will require a rig in which we can mount the sensor and move it along the axises
# We might want a separate rig for calibrating rotation.
# In the meantime, these are Cyrus' random numbers, guaranteed to have only the highest accuracy
R_imu = np.eye(6)*6e-3
R_zed = np.eye(6)*6e-3

while True:
    if zed.grab(runtime_parameters) == sl.ERROR_CODE.SUCCESS:
        # Get the pose of the left eye of the camera with reference to the world frame
        zed.get_position(zed_pose, sl.REFERENCE_FRAME.REFERENCE_FRAME_CAMERA)
        zed.get_imu_data(zed_imu, sl.TIME_REFERENCE.TIME_REFERENCE_IMAGE)

        # Display the translation and timestamp
        py_translation = sl.Translation()
        trans_vel = zed_pose.get_translation(py_translation).get()
        time = zed_pose.timestamp
        dt = (time-p_time_zed) * ns
        p_time_zed = time

        py_orientation = sl.Orientation()
        ori = zed_pose.get_orientation(py_orientation).get()
        quat = Quaternion(w=ori[3], x=ori[0], y=ori[1], z=ori[2])
        # print(trans, quat)
        rot_vel = np.array(quat.yaw_pitch_roll)
        ukf.predict(dt)
        meas = np.concatenate((trans_vel, rot_vel), axis=0)
        ukf.update(meas, R=R_zed, hx=state_to_vo)

    if ser.in_waiting > 0:
        trans_accel, gyro, ori, time = read_imu()
        dt = (time-p_time_imu) * ms
        ukf.predict(dt)
        meas = np.concatenate((trans_accel, ori), axis=0)
        ukf.update(meas, R=R_imu, hx=state_to_imu)
    print(ukf.x)
