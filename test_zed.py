import pyzed.sl as sl

ns = 1e9

def init_zed():
    # Init ZED Camera
    zed = sl.Camera()
    init_params = sl.InitParameters()
    # Use HD720 video mode (default fps: 60)
    init_params.camera_resolution = sl.RESOLUTION.RESOLUTION_HD720
    # Use a right-handed Y-up coordinate system
    init_params.coordinate_system = sl.COORDINATE_SYSTEM.COORDINATE_SYSTEM_RIGHT_HANDED_Y_UP
    init_params.coordinate_units = sl.UNIT.UNIT_METER  # Set units in meters

    err = zed.open(init_params)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Enable positional tracking with default parameters
    py_transform = sl.Transform()  # First create a Transform object for TrackingParameters object
    tracking_parameters = sl.TrackingParameters(init_pos=py_transform)
    tracking_parameters.enable_spatial_memory = False
    tracking_parameters.enable_imu_fusion = False
    err = zed.enable_tracking(tracking_parameters)
    if err != sl.ERROR_CODE.SUCCESS:
        exit(1)

    # Track the camera position during 1000 frames
    i = 0
    zed_pose = sl.Pose()
    zed_imu = sl.IMUData()
    runtime_parameters = sl.RuntimeParameters()
    return zed, zed_pose, zed_imu, runtime_parameters

zed, zed_pose, zed_imu, runtime_parameters = init_zed()

py_translation = sl.Translation()
p_time = zed_pose.timestamp
while True:
    if zed.grab(runtime_parameters) != sl.ERROR_CODE.SUCCESS:
        continue
    # Get the pose of the left eye of the camera with reference to the world frame
    zed.get_position(zed_pose, sl.REFERENCE_FRAME.REFERENCE_FRAME_CAMERA)
    zed.get_imu_data(zed_imu, sl.TIME_REFERENCE.TIME_REFERENCE_IMAGE)

    # Display the translation and timestamp
    py_translation = sl.Translation()
    trans = zed_pose.get_translation(py_translation).get()
    time = zed_pose.timestamp
    dt = (time-p_time) * ns
    p_time = time

    py_orientation = sl.Orientation()
    zed_ori = zed_pose.get_orientation(py_orientation).get()

    # We don't have an imu cause we have an old zed
    #Display the IMU acceleratoin
    #acceleration = [0,0,0]
    #zed_imu.get_linear_acceleration(acceleration)

    #Display the IMU angular velocity
    #a_velocity = [0,0,0]
    #zed_imu.get_angular_velocity(a_velocity)

    # Display the IMU orientation quaternion
    #imu_orientation = sl.Orientation()
    #imu_ori = zed_imu.get_orientation(imu_orientation).get()
    print(trans, zed_ori)

