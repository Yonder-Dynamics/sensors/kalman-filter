Accel:7.81, -42.94, 1021.38 mg

Gyro:6.43, 18.51, 1.38 deg/s

Mag:62,-365,334 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.02 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.87, -0.53, -2.53

Hardware Yaw, Pitch, Roll: 93.92, -1.16, -3.23

236.0,93.87,-0.53,-2.53,93.92,-1.16,-3.23

Accel:13.66, -39.53, 1016.02 mg

Gyro:-2.30, -3.21, 0.15 deg/s

Mag:65,-373,338 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.02 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.93, -0.57, -2.43

Hardware Yaw, Pitch, Roll: 93.97, -1.06, -3.16

236.0,93.93,-0.57,-2.43,93.97,-1.06,-3.16

Accel:20.50, -33.18, 1007.23 mg

Gyro:8.87, 4.74, 3.82 deg/s

Mag:62,-367,343 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.10, -0.73, -2.15

Hardware Yaw, Pitch, Roll: 94.17, -0.93, -2.95

236.1,94.10,-0.73,-2.15,94.17,-0.93,-2.95

Accel:4.88, -34.65, 1015.53 mg

Gyro:0.46, 0.76, 1.99 deg/s

Mag:64,-375,338 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.65

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.26, -0.56, -1.80

Hardware Yaw, Pitch, Roll: 94.31, -0.80, -2.70

236.1,94.26,-0.56,-1.80,94.31,-0.80,-2.70

Accel:2.44, -49.29, 1022.36 mg

Gyro:2.91, 0.61, -0.76 deg/s

Mag:62,-365,340 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.65

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.27, -0.40, -2.05

Hardware Yaw, Pitch, Roll: 94.33, -0.73, -2.69

236.2,94.27,-0.40,-2.05,94.33,-0.73,-2.69

Accel:3.90, -34.65, 1014.55 mg

Gyro:-2.14, -1.53, -1.07 deg/s

Mag:68,-356,338 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.65

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.16, -0.36, -2.11

Hardware Yaw, Pitch, Roll: 94.25, -0.76, -2.67

236.3,94.16,-0.36,-2.11,94.25,-0.76,-2.67

Accel:12.20, -29.77, 1016.99 mg

Gyro:0.76, 1.07, 0.00 deg/s

Mag:61,-365,332 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.08, -0.48, -1.95

Hardware Yaw, Pitch, Roll: 94.20, -0.80, -2.65

236.3,94.08,-0.48,-1.95,94.20,-0.80,-2.65

Accel:6.34, -36.60, 1016.50 mg

Gyro:0.76, -1.07, 0.61 deg/s

Mag:68,-358,332 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.09, -0.48, -1.87

Hardware Yaw, Pitch, Roll: 94.23, -0.78, -2.64

236.4,94.09,-0.48,-1.87,94.23,-0.78,-2.64

Accel:8.30, -34.16, 1022.85 mg

Gyro:0.61, -0.76, -0.15 deg/s

Mag:55,-364,344 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.13, -0.48, -1.88

Hardware Yaw, Pitch, Roll: 94.25, -0.80, -2.60

236.4,94.13,-0.48,-1.88,94.25,-0.80,-2.60

Accel:3.42, -35.62, 1011.62 mg

Gyro:-1.68, -5.20, -2.60 deg/s

Mag:67,-362,337 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.01, -0.53, -1.96

Hardware Yaw, Pitch, Roll: 94.17, -0.93, -2.59

236.5,94.01,-0.53,-1.96,94.17,-0.93,-2.59

Accel:16.59, -35.62, 1019.92 mg

Gyro:-2.14, -1.53, -1.07 deg/s

Mag:59,-359,345 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.92, -0.72, -2.06

Hardware Yaw, Pitch, Roll: 94.05, -1.15, -2.69

236.5,93.92,-0.72,-2.06,94.05,-1.15,-2.69

Accel:18.06, -42.94, 1006.26 mg

Gyro:1.38, 1.53, 1.84 deg/s

Mag:67,-354,338 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.89, -0.86, -2.19

Hardware Yaw, Pitch, Roll: 94.05, -1.15, -2.72

236.6,93.89,-0.86,-2.19,94.05,-1.15,-2.72

Accel:15.62, -28.30, 1016.99 mg

Gyro:5.05, 2.60, 1.53 deg/s

Mag:61,-358,337 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.06, -0.78, -2.01

Hardware Yaw, Pitch, Roll: 94.20, -0.97, -2.54

236.6,94.06,-0.78,-2.01,94.20,-0.97,-2.54

Accel:10.25, -24.40, 1028.22 mg

Gyro:-1.22, -3.21, 1.22 deg/s

Mag:70,-359,344 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.01 qz = -0.65

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.19, -0.72, -1.65

Hardware Yaw, Pitch, Roll: 94.30, -0.99, -2.43

236.7,94.19,-0.72,-1.65,94.30,-0.99,-2.43

Accel:8.30, -40.02, 1018.94 mg

Gyro:-0.76, -6.73, -2.30 deg/s

Mag:64,-359,335 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.65 Qz = -0.76

Software yaw, pitch, roll: 94.15, -0.84, -1.74

Hardware Yaw, Pitch, Roll: 94.27, -1.17, -2.46

236.7,94.15,-0.84,-1.74,94.27,-1.17,-2.46

Accel:10.25, -40.99, 1003.82 mg

Gyro:-3.82, -3.98, -3.21 deg/s

Mag:59,-359,343 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.07, -0.83, -2.16

Hardware Yaw, Pitch, Roll: 94.19, -1.33, -2.52

236.8,94.07,-0.83,-2.16,94.19,-1.33,-2.52

Accel:23.91, -39.04, 1020.90 mg

Gyro:0.31, -1.68, -5.20 deg/s

Mag:73,-361,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.91, -1.03, -2.28

Hardware Yaw, Pitch, Roll: 94.08, -1.37, -2.61

236.8,93.91,-1.03,-2.28,94.08,-1.37,-2.61

Accel:24.40, -30.74, 1006.74 mg

Gyro:-0.76, 0.46, -0.31 deg/s

Mag:68,-361,331 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.76, -1.22, -2.13

Hardware Yaw, Pitch, Roll: 93.88, -1.38, -2.65

236.9,93.76,-1.22,-2.13,93.88,-1.38,-2.65

Accel:23.42, -21.96, 1012.60 mg

Gyro:-1.22, -3.37, -2.91 deg/s

Mag:68,-365,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.70, -1.33, -1.78

Hardware Yaw, Pitch, Roll: 93.81, -1.52, -2.64

236.9,93.70,-1.33,-1.78,93.81,-1.52,-2.64

Accel:30.26, -35.14, 1014.06 mg

Gyro:1.68, 0.31, 0.31 deg/s

Mag:70,-362,343 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.73, -1.39, -1.78

Hardware Yaw, Pitch, Roll: 93.81, -1.53, -2.61

237.0,93.73,-1.39,-1.78,93.81,-1.53,-2.61

Accel:27.82, -31.72, 1019.92 mg

Gyro:3.06, 4.74, 2.30 deg/s

Mag:74,-350,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.70, -1.53, -1.69

Hardware Yaw, Pitch, Roll: 93.92, -1.34, -2.44

237.0,93.70,-1.53,-1.69,93.92,-1.34,-2.44

Accel:17.08, -21.47, 1016.02 mg

Gyro:2.14, -1.68, 0.61 deg/s

Mag:68,-356,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.77, -1.29, -1.53

Hardware Yaw, Pitch, Roll: 93.97, -1.26, -2.29

237.1,93.77,-1.29,-1.53,93.97,-1.26,-2.29

Accel:18.06, -28.30, 1019.92 mg

Gyro:-2.91, -4.28, 0.00 deg/s

Mag:67,-362,323 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.80, -1.26, -1.50

Hardware Yaw, Pitch, Roll: 93.96, -1.42, -2.35

237.1,93.80,-1.26,-1.50,93.96,-1.42,-2.35

Accel:29.28, -36.60, 1004.79 mg

Gyro:3.98, 3.21, 0.31 deg/s

Mag:76,-358,332 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.79, -1.39, -1.73

Hardware Yaw, Pitch, Roll: 93.97, -1.46, -2.35

237.2,93.79,-1.39,-1.73,93.97,-1.46,-2.35

Accel:24.40, -31.72, 1007.23 mg

Gyro:2.91, 4.44, 3.37 deg/s

Mag:73,-362,340 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.91, -1.25, -1.61

Hardware Yaw, Pitch, Roll: 93.98, -1.32, -2.21

237.2,93.91,-1.25,-1.61,93.98,-1.32,-2.21

Accel:13.66, -29.28, 1020.90 mg

Gyro:0.00, 0.76, 1.38 deg/s

Mag:70,-359,341 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.00, -1.07, -1.61

Hardware Yaw, Pitch, Roll: 94.16, -1.11, -2.09

237.3,94.00,-1.07,-1.61,94.16,-1.11,-2.09

Accel:13.18, -38.55, 1022.36 mg

Gyro:-0.46, -3.52, -1.38 deg/s

Mag:71,-359,335 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.00, -0.97, -1.71

Hardware Yaw, Pitch, Roll: 94.09, -1.22, -2.17

237.3,94.00,-0.97,-1.71,94.09,-1.22,-2.17

Accel:17.08, -32.21, 1022.85 mg

Gyro:-4.28, -3.37, -2.30 deg/s

Mag:61,-364,341 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.95, -0.99, -1.99

Hardware Yaw, Pitch, Roll: 94.02, -1.31, -2.30

237.4,93.95,-0.99,-1.99,94.02,-1.31,-2.30

Accel:27.33, -33.18, 1016.99 mg

Gyro:1.38, 4.28, 0.92 deg/s

Mag:64,-353,324 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.85, -1.21, -2.03

Hardware Yaw, Pitch, Roll: 93.93, -1.47, -2.52

237.4,93.85,-1.21,-2.03,93.93,-1.47,-2.52

Accel:25.38, -30.74, 1010.16 mg

Gyro:1.07, 3.98, 1.22 deg/s

Mag:65,-369,340 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.89, -1.25, -1.98

Hardware Yaw, Pitch, Roll: 93.90, -1.51, -2.55

237.5,93.89,-1.25,-1.98,93.90,-1.51,-2.55

Accel:28.30, -35.14, 1014.55 mg

Gyro:-0.46, -0.61, -1.53 deg/s

Mag:71,-361,324 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.79, -1.46, -1.98

Hardware Yaw, Pitch, Roll: 93.87, -1.55, -2.64

237.6,93.79,-1.46,-1.98,93.87,-1.55,-2.64

Accel:29.28, -31.23, 1022.36 mg

Gyro:-1.68, -0.76, -0.92 deg/s

Mag:77,-362,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.78, -1.51, -1.95

Hardware Yaw, Pitch, Roll: 93.82, -1.55, -2.66

237.6,93.78,-1.51,-1.95,93.82,-1.55,-2.66

Accel:26.35, -34.16, 1013.09 mg

Gyro:-0.61, -1.99, -0.61 deg/s

Mag:67,-361,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.78, -1.51, -1.95

Hardware Yaw, Pitch, Roll: 93.83, -1.53, -2.67

237.7,93.78,-1.51,-1.95,93.83,-1.53,-2.67

Accel:25.38, -30.26, 1013.09 mg

Gyro:-1.53, -1.68, -0.15 deg/s

Mag:74,-362,337 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.87, -1.47, -1.85

Hardware Yaw, Pitch, Roll: 93.83, -1.52, -2.62

237.7,93.87,-1.47,-1.85,93.83,-1.52,-2.62

Accel:26.84, -37.09, 1018.46 mg

Gyro:-1.07, -1.68, 0.00 deg/s

Mag:76,-370,332 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.57, -1.92

Hardware Yaw, Pitch, Roll: 93.88, -1.57, -2.65

237.8,93.81,-1.57,-1.92,93.88,-1.57,-2.65

Accel:25.38, -40.50, 1013.58 mg

Gyro:-3.67, -2.30, -2.60 deg/s

Mag:64,-367,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.50, -2.03

Hardware Yaw, Pitch, Roll: 93.88, -1.52, -2.63

237.8,93.81,-1.50,-2.03,93.88,-1.52,-2.63

Accel:32.21, -40.50, 1020.41 mg

Gyro:2.45, 6.27, 1.53 deg/s

Mag:70,-359,324 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.50, -2.12

Hardware Yaw, Pitch, Roll: 93.85, -1.48, -2.67

237.9,93.81,-1.50,-2.12,93.85,-1.48,-2.67

Accel:31.23, -39.04, 1006.74 mg

Gyro:1.84, 0.46, 0.00 deg/s

Mag:67,-362,338 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.03 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.78, -1.59, -2.12

Hardware Yaw, Pitch, Roll: 93.82, -1.51, -2.66

237.9,93.78,-1.59,-2.12,93.82,-1.51,-2.66

Accel:30.74, -35.14, 1013.58 mg

Gyro:0.76, 3.21, 0.61 deg/s

Mag:71,-362,337 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.83, -1.60, -2.01

Hardware Yaw, Pitch, Roll: 93.87, -1.42, -2.57

238.0,93.83,-1.60,-2.01,93.87,-1.42,-2.57

Accel:24.89, -37.09, 1014.55 mg

Gyro:0.46, -0.31, 0.76 deg/s

Mag:65,-361,321 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.72, -1.60, -1.92

Hardware Yaw, Pitch, Roll: 93.85, -1.40, -2.52

238.0,93.72,-1.60,-1.92,93.85,-1.40,-2.52

Accel:25.38, -34.16, 1019.92 mg

Gyro:0.00, -0.46, -0.92 deg/s

Mag:73,-351,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.71, -1.58, -1.96

Hardware Yaw, Pitch, Roll: 93.87, -1.42, -2.51

238.1,93.71,-1.58,-1.96,93.87,-1.42,-2.51

Accel:29.28, -36.11, 1016.02 mg

Gyro:-1.07, -0.31, 1.07 deg/s

Mag:74,-375,318 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.72, -1.59, -1.96

Hardware Yaw, Pitch, Roll: 93.83, -1.52, -2.52

238.1,93.72,-1.59,-1.96,93.83,-1.52,-2.52

Accel:28.79, -33.67, 1012.11 mg

Gyro:0.76, 1.07, -0.61 deg/s

Mag:76,-364,328 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.65, -1.62, -1.88

Hardware Yaw, Pitch, Roll: 93.84, -1.43, -2.53

238.2,93.65,-1.62,-1.88,93.84,-1.43,-2.53

Accel:27.82, -37.09, 1010.16 mg

Gyro:1.22, -0.31, 1.68 deg/s

Mag:82,-353,331 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.64, -1.63, -1.87

Hardware Yaw, Pitch, Roll: 93.81, -1.45, -2.48

238.2,93.64,-1.63,-1.87,93.81,-1.45,-2.48

Accel:31.23, -39.53, 1016.50 mg

Gyro:3.21, 2.91, 1.07 deg/s

Mag:76,-361,331 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.65, -1.64, -1.95

Hardware Yaw, Pitch, Roll: 93.86, -1.40, -2.37

238.3,93.65,-1.64,-1.95,93.86,-1.40,-2.37

Accel:23.42, -33.18, 1013.58 mg

Gyro:-0.61, -1.22, 1.38 deg/s

Mag:68,-354,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.70, -1.56, -1.89

Hardware Yaw, Pitch, Roll: 93.91, -1.36, -2.36

238.3,93.70,-1.56,-1.89,93.91,-1.36,-2.36

Accel:29.77, -31.23, 1016.02 mg

Gyro:0.92, 1.07, 1.38 deg/s

Mag:73,-365,320 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.66, -1.58, -1.85

Hardware Yaw, Pitch, Roll: 93.93, -1.39, -2.34

238.4,93.66,-1.58,-1.85,93.93,-1.39,-2.34

Accel:26.35, -30.26, 1018.94 mg

Gyro:-0.76, -0.15, 0.00 deg/s

Mag:76,-361,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.72, -1.59, -1.79

Hardware Yaw, Pitch, Roll: 93.97, -1.41, -2.35

238.4,93.72,-1.59,-1.79,93.97,-1.41,-2.35

Accel:25.86, -33.18, 1017.48 mg

Gyro:0.31, -1.84, 0.46 deg/s

Mag:65,-362,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.73, -1.48, -1.74

Hardware Yaw, Pitch, Roll: 93.98, -1.38, -2.27

238.5,93.73,-1.48,-1.74,93.98,-1.38,-2.27

Accel:27.82, -34.16, 1018.94 mg

Gyro:-1.22, -1.68, -1.84 deg/s

Mag:67,-364,334 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.71, -1.50, -1.85

Hardware Yaw, Pitch, Roll: 93.91, -1.46, -2.33

238.5,93.71,-1.50,-1.85,93.91,-1.46,-2.33

Accel:31.72, -32.70, 1018.46 mg

Gyro:3.82, 10.86, 5.36 deg/s

Mag:70,-348,328 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.76, -1.47, -1.86

Hardware Yaw, Pitch, Roll: 93.87, -1.47, -2.42

238.6,93.76,-1.47,-1.86,93.87,-1.47,-2.42

Accel:27.82, -35.62, 1011.14 mg

Gyro:0.15, -0.76, -0.46 deg/s

Mag:67,-362,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.77, -1.49, -1.91

Hardware Yaw, Pitch, Roll: 93.90, -1.42, -2.43

238.6,93.77,-1.49,-1.91,93.90,-1.42,-2.43

Accel:33.18, -32.70, 1016.99 mg

Gyro:3.37, 8.87, 2.60 deg/s

Mag:64,-358,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.72, -1.58, -1.88

Hardware Yaw, Pitch, Roll: 93.87, -1.46, -2.43

238.7,93.72,-1.58,-1.88,93.87,-1.46,-2.43

Accel:28.30, -35.62, 1016.99 mg

Gyro:-1.07, -1.07, -0.15 deg/s

Mag:68,-365,328 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.73, -1.55, -1.85

Hardware Yaw, Pitch, Roll: 93.85, -1.44, -2.40

238.7,93.73,-1.55,-1.85,93.85,-1.44,-2.40

Accel:27.82, -34.16, 1014.06 mg

Gyro:-0.61, -1.38, -0.46 deg/s

Mag:73,-354,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.79, -1.56, -1.92

Hardware Yaw, Pitch, Roll: 93.88, -1.49, -2.44

238.8,93.79,-1.56,-1.92,93.88,-1.49,-2.44

Accel:30.74, -30.26, 1018.46 mg

Gyro:-0.92, -0.15, 0.00 deg/s

Mag:68,-359,331 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.75, -1.65, -1.92

Hardware Yaw, Pitch, Roll: 93.86, -1.52, -2.46

238.9,93.75,-1.65,-1.92,93.86,-1.52,-2.46

Accel:31.23, -33.67, 1012.11 mg

Gyro:1.84, 1.07, 0.46 deg/s

Mag:65,-359,335 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.65, -1.86

Hardware Yaw, Pitch, Roll: 93.86, -1.56, -2.51

238.9,93.81,-1.65,-1.86,93.86,-1.56,-2.51

Accel:28.79, -39.53, 1013.09 mg

Gyro:1.84, 2.30, 2.14 deg/s

Mag:70,-359,324 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.87, -1.60, -1.86

Hardware Yaw, Pitch, Roll: 93.93, -1.47, -2.41

239.0,93.87,-1.60,-1.86,93.93,-1.47,-2.41

Accel:24.40, -30.74, 1017.97 mg

Gyro:0.92, 1.07, -0.76 deg/s

Mag:73,-356,341 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.96, -1.46, -1.84

Hardware Yaw, Pitch, Roll: 93.97, -1.40, -2.41

239.0,93.96,-1.46,-1.84,93.97,-1.40,-2.41

Accel:24.89, -32.21, 1017.48 mg

Gyro:0.76, 0.61, 0.92 deg/s

Mag:59,-353,332 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.85, -1.46, -1.81

Hardware Yaw, Pitch, Roll: 93.96, -1.36, -2.36

239.1,93.85,-1.46,-1.81,93.96,-1.36,-2.36

Accel:23.42, -29.77, 1011.62 mg

Gyro:0.15, 1.07, 1.38 deg/s

Mag:61,-362,324 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.99, -1.33, -1.74

Hardware Yaw, Pitch, Roll: 94.00, -1.31, -2.35

239.1,93.99,-1.33,-1.74,94.00,-1.31,-2.35

Accel:21.96, -28.79, 1017.48 mg

Gyro:1.07, -0.15, -0.46 deg/s

Mag:56,-359,331 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.11, -1.24, -1.73

Hardware Yaw, Pitch, Roll: 94.04, -1.28, -2.35

239.2,94.11,-1.24,-1.73,94.04,-1.28,-2.35

Accel:18.54, -29.28, 1021.87 mg

Gyro:-2.45, -5.20, -1.68 deg/s

Mag:67,-365,334 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.02, -1.32, -1.68

Hardware Yaw, Pitch, Roll: 94.03, -1.42, -2.40

239.2,94.02,-1.32,-1.68,94.03,-1.42,-2.40

Accel:26.84, -34.65, 1015.53 mg

Gyro:-1.07, -1.22, 0.00 deg/s

Mag:67,-358,341 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.96, -1.40, -1.78

Hardware Yaw, Pitch, Roll: 93.98, -1.51, -2.42

239.3,93.96,-1.40,-1.78,93.98,-1.51,-2.42

Accel:26.84, -35.62, 1012.60 mg

Gyro:-0.31, 0.15, 0.15 deg/s

Mag:64,-364,331 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.92, -1.48, -1.89

Hardware Yaw, Pitch, Roll: 93.95, -1.52, -2.42

239.3,93.92,-1.48,-1.89,93.95,-1.52,-2.42

Accel:25.38, -32.70, 1016.02 mg

Gyro:3.06, -0.76, 0.92 deg/s

Mag:76,-362,341 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.88, -1.53, -1.86

Hardware Yaw, Pitch, Roll: 93.95, -1.55, -2.37

239.4,93.88,-1.53,-1.86,93.95,-1.55,-2.37

Accel:27.33, -34.16, 1017.97 mg

Gyro:0.00, -0.76, 0.15 deg/s

Mag:76,-353,328 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.84, -1.60, -1.86

Hardware Yaw, Pitch, Roll: 93.95, -1.61, -2.36

239.4,93.84,-1.60,-1.86,93.95,-1.61,-2.36

Accel:24.40, -31.23, 1015.04 mg

Gyro:-1.84, -1.68, -0.46 deg/s

Mag:68,-362,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.85, -1.51, -1.88

Hardware Yaw, Pitch, Roll: 93.91, -1.58, -2.36

239.5,93.85,-1.51,-1.88,93.91,-1.58,-2.36

Accel:23.91, -30.26, 1014.55 mg

Gyro:-0.46, -2.14, -1.53 deg/s

Mag:68,-350,327 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.79, -1.48, -1.80

Hardware Yaw, Pitch, Roll: 93.92, -1.52, -2.33

239.5,93.79,-1.48,-1.80,93.92,-1.52,-2.33

Accel:26.84, -34.16, 1016.99 mg

Gyro:-1.22, -1.38, -0.31 deg/s

Mag:68,-359,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.46, -1.81

Hardware Yaw, Pitch, Roll: 93.89, -1.52, -2.29

239.6,93.81,-1.46,-1.81,93.89,-1.52,-2.29

Accel:29.77, -36.60, 1021.38 mg

Gyro:-0.76, 1.38, 0.15 deg/s

Mag:77,-359,332 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.85, -1.53, -1.94

Hardware Yaw, Pitch, Roll: 93.94, -1.53, -2.35

239.6,93.85,-1.53,-1.94,93.94,-1.53,-2.35

Accel:27.33, -32.21, 1009.67 mg

Gyro:0.00, 0.31, 0.15 deg/s

Mag:73,-361,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.86, -1.50, -1.95

Hardware Yaw, Pitch, Roll: 93.93, -1.52, -2.33

239.7,93.86,-1.50,-1.95,93.93,-1.52,-2.33

Accel:28.30, -34.65, 1012.11 mg

Gyro:0.15, 0.76, 1.07 deg/s

Mag:61,-358,330 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.77

Software yaw, pitch, roll: 93.81, -1.56, -1.85

Hardware Yaw, Pitch, Roll: 93.95, -1.48, -2.32

239.7,93.81,-1.56,-1.85,93.95,-1.48,-2.32

Accel:29.77, -30.74, 1016.99 mg

Gyro:1.22, 3.37, 1.99 deg/s

Mag:68,-354,332 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.92, -1.50, -1.78

Hardware Yaw, Pitch, Roll: 94.04, -1.36, -2.24

239.8,93.92,-1.50,-1.78,94.04,-1.36,-2.24

Accel:20.01, -37.58, 1012.11 mg

Gyro:-0.31, -0.76, 1.84 deg/s

Mag:68,-356,328 mG

Software quaternions (ENU):

q0 = -0.76 qx = 0.00 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 94.02, -1.38, -1.82

Hardware Yaw, Pitch, Roll: 94.12, -1.31, -2.22

239.8,94.02,-1.38,-1.82,94.12,-1.31,-2.22

Accel:22.45, -34.65, 1021.87 mg

Gyro:0.00, -0.61, -0.61 deg/s

Mag:62,-361,328 mG

Software quaternions (ENU):

q0 = -0.77 qx = 0.01 qy = 0.02 qz = -0.64

Hardware quaternions (NED):

Q0 = 0.01 Qx = 0.02 Qy = -0.64 Qz = -0.76

Software yaw, pitch, roll: 93.98, -1.40, -1.97

Hardware Yaw, Pitch, Roll: 94.16, -1.35, -2.21

239.9,93.98,-1.40,-1.97,94.16,-1.35,-2.21


